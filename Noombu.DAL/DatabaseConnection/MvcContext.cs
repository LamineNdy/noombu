﻿using System.Data.Entity;
using Noombu.DAL.Model;

namespace Noombu.DAL.DatabaseConnection
{    
    public class NoombuDataContext : DbContext
    {
        public DbSet<GroupModel> GroupModel { get; set; }
        public DbSet<CommentModel> CommentModel { get; set; }
        public DbSet<PostModel> PostModel { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }
         public NoombuDataContext()
            : base("NoombuDatabase")
        {
        }
    }

}
