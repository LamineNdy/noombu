﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Noombu.DAL.Model
{
    
    [Table("UserProfile")]
    public class UserProfile
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string UserLastName { get; set; }
        public string UserFirstName { get; set; }
        //public DateTime DateOfBirth {get;set;}    
        public String EmailAdress { get; set; }
        public String Departement { get; set; }
        public String Title { get; set; }
        public String Gender { get; set; }   
        public virtual List<UserProfile> Following { get; set; }
        public virtual List<PostModel> LikedPost { get; set; }
        public virtual List<GroupModel> OwnedGroup { get; set; }
    }

}
