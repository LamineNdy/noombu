﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Noombu.DAL.Model
{
    public class CommentModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CommentId { get; set; }
        //[ForeignKey("UserId")] public int UserId;
        public String Comment { get; set; }
        public virtual PostModel PostModel { get; set; }
        public virtual UserProfile UserProfile { get; set; }
        public DateTime CommentTime { get; set; }
    }
}
