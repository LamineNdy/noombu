﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Noombu.DAL.Model
{
    public class GroupModel
    {
        [Key]       
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int GroupId { get; set; }
        //[ForeignKey("UserId")] public int OwnerId { get; set; }
        public virtual UserProfile GroupOwner { get; set; }
        public virtual List<UserProfile> UsersGroup { get; set; }
        public String Name { get; set; }
    }
}
