﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Noombu.DAL.Model
{
   public class PostModel
    {
        //[ForeignKey("UserId")] public int UserId;
       [Key]
       [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
       public int Postid { get; set; }
        public virtual UserProfile UserProfile{ get; set; }
        public String Content { get; set; }
        public String Image { get; set; }
        public int Likes { get; set; }
        public virtual List<CommentModel> Comments { get; set; }
        public virtual List<UserProfile> Likers { get; set; }
        //[ForeignKey("GroupId")]public int GroupId;
        public virtual GroupModel GroupModel { get; set; }      
    }
}
