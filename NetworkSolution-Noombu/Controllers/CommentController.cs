﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Noombu.DAL.Model;
using Noombu.DAL.DatabaseConnection;

namespace NetworkSolution_Noombu.Controllers
{
    public class CommentController : Controller
    {
        private NoombuDataContext db = new NoombuDataContext();

        //
        // GET: /Comment/

        public ActionResult Index()
        {
            return View(db.CommentModel.ToList());
        }

        //
        // GET: /Comment/Details/5

        public ActionResult Details(int id = 0)
        {
            CommentModel commentmodel = db.CommentModel.Find(id);
            if (commentmodel == null)
            {
                return HttpNotFound();
            }
            return View(commentmodel);
        }

        //
        // GET: /Comment/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Comment/Create

        [HttpPost]
        public ActionResult Create(CommentModel commentmodel)
        {
            if (ModelState.IsValid)
            {
                db.CommentModel.Add(commentmodel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(commentmodel);
        }

        //
        // GET: /Comment/Edit/5

        public ActionResult Edit(int id = 0)
        {
            CommentModel commentmodel = db.CommentModel.Find(id);
            if (commentmodel == null)
            {
                return HttpNotFound();
            }
            return View(commentmodel);
        }

        //
        // POST: /Comment/Edit/5

        [HttpPost]
        public ActionResult Edit(CommentModel commentmodel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(commentmodel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(commentmodel);
        }

        //
        // GET: /Comment/Delete/5

        public ActionResult Delete(int id = 0)
        {
            CommentModel commentmodel = db.CommentModel.Find(id);
            if (commentmodel == null)
            {
                return HttpNotFound();
            }
            return View(commentmodel);
        }

        //
        // POST: /Comment/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            CommentModel commentmodel = db.CommentModel.Find(id);
            db.CommentModel.Remove(commentmodel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}