﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Noombu.DAL.Model;
using Noombu.DAL.DatabaseConnection;

namespace NetworkSolution_Noombu.Controllers
{
    public class PostController : Controller
    {
        private NoombuDataContext db = new NoombuDataContext();       

        //
        // GET: /Post/

        public ActionResult Index()
        {
            return View(db.PostModel.ToList());
        }

        //
        // GET: /Post/Details/5

        public ActionResult Details(int id = 0)
        {
            PostModel postmodel = db.PostModel.Find(id);
            if (postmodel == null)
            {
                return HttpNotFound();
            }
            return View(postmodel);
        }

        //
        // GET: /Post/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Post/Create

        [HttpPost]
        public ActionResult Create(PostModel postmodel)
        {
            if (ModelState.IsValid)
            {
                var user = db.UserProfiles.ToList();
                foreach (var u in user)
                {
                    if (u.UserName == User.Identity.Name)
                    {                        
                        postmodel.UserProfile = u;
                        postmodel.Likers=new List<UserProfile>();                          
                    }

                }
                db.PostModel.Add(postmodel);
                db.SaveChanges();
                return RedirectToAction("Index","Home");
            }

            return View(postmodel);
        }

        //
        // GET: /Post/Edit/5

        public ActionResult Edit(int id = 0)
        {
            PostModel postmodel = db.PostModel.Find(id);
            if (postmodel == null)
            {
                return HttpNotFound();
            }
            return View(postmodel);
        }

        //
        // POST: /Post/Edit/5

        [HttpPost]
        public ActionResult Edit(PostModel postmodel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(postmodel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index","Home");
            }
            return View(postmodel);
        }

        // POST: /Post/Like/5

        public ActionResult Like(int id =0)
        {
            PostModel postmodel = db.PostModel.Find(id);

            if (postmodel != null)
            {
                bool liked = false;
                UserProfile liker=new UserProfile();
                foreach (UserProfile t in postmodel.Likers)
                {
                   
                    if (t.UserName == User.Identity.Name )
                    {
                        liker = t;
                        liked = true;                         
                    }
                }
                if (liked == false)
                {
                    foreach (var t in db.UserProfiles.ToList())
                    {
                       
                        if (t.UserName == User.Identity.Name)
                        {
                            liker = t;

                        }
                    }
                  
                    postmodel.Likers.Add(liker);
                    liker.LikedPost.Add(postmodel);
                }
                else
                {
                    postmodel.Likers.Remove(liker);
                    liker.LikedPost.Remove(postmodel);
                    
                }
                db.Entry(postmodel).State = EntityState.Modified;
                db.SaveChanges();
               return RedirectToAction("Index");
            }
            else
            {
                postmodel=new PostModel();
            }
            return View(postmodel);
        }

        //
        // GET: /Post/Delete/5

        public ActionResult Delete(int id = 0)
        {
            PostModel postmodel = db.PostModel.Find(id);
            if (postmodel == null)
            {
                return HttpNotFound();
            }
            return View(postmodel);
        }

        //
        // POST: /Post/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            PostModel postmodel = db.PostModel.Find(id);
            db.PostModel.Remove(postmodel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}