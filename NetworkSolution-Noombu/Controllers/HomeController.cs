﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using NetworkSolution_Noombu.Filters;
using NetworkSolution_Noombu.Models;
using Noombu.DAL.DatabaseConnection;
using Noombu.DAL.Model;

namespace NetworkSolution_Noombu.Controllers
{
     [Authorize]    
    public class HomeController : Controller
    {
         private NoombuDataContext db = new NoombuDataContext();
        
         
        public ActionResult Index()
        {
            ViewBag.Message = "";

            return View(db.PostModel.ToList());
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Coworkers()
        {
            return View(db.UserProfiles.ToList());
    
        }

        public ActionResult Groups()
        {
            return View(db.GroupModel.ToList());

        }

         [HttpPost]
        public ActionResult SearchStatus(PostModel search)
         {
             var foundStatus = new List<PostModel>();
             if (ModelState.IsValid)
             {
                 var posts = db.PostModel.ToList();
                 foreach (var p in posts)
                 {
                     if (p.Content.Contains(search.Content))
                     {
                         foundStatus .Add(p);
                     }

                 }
                 ViewBag.Message =search.Content;
                
             }
             return View("SearchResult",foundStatus);
         }

         public ActionResult SearchStatus()
         {
             return View();
         }


         public ActionResult SearchWorker()
         {
             return View();
         }

          [HttpPost]
         public ActionResult SearchWorker(UserProfile us)
         {
             var foundWorker = new List<UserProfile>();
             if (ModelState.IsValid)
             {
                 var posts = db.UserProfiles.ToList();
                 foreach (var p in posts)
                 {
                     if (p.UserName.Contains(us.UserName) || p.EmailAdress.Contains(us.EmailAdress))
                     {
                         foundWorker.Add(p);
                     }

                 }
                 ViewBag.Message = us.UserName;

             }
             return View("SearchWorkerResult", foundWorker);
         }
    }
}
