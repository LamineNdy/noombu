﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Noombu.DAL.Model;
using Noombu.DAL.DatabaseConnection;

namespace NetworkSolution_Noombu.Controllers
{
    public class GroupController : Controller
    {
        private NoombuDataContext db = new NoombuDataContext();       
        //
        // GET: /Group/

        public ActionResult Index()
        {
            return View(db.GroupModel.ToList());
        }

        //
        // GET: /Group/Details/5

        public ActionResult Details(int id = 0)
        {
            GroupModel groupmodel = db.GroupModel.Find(id);
            if (groupmodel == null)
            {
                return HttpNotFound();
            }
            return View(groupmodel);
        }

        //
        // GET: /Group/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Group/Create

        [HttpPost]
        public ActionResult Create(GroupModel groupmodel)
        {
            if (ModelState.IsValid)
            {
                var user = db.UserProfiles.ToList();
                foreach (var u in user)
                {
                    if (u.UserName == User.Identity.Name)
                    {                       
                        groupmodel.GroupOwner = u;                      
                        groupmodel.UsersGroup = new List<UserProfile>();
                        groupmodel.UsersGroup.Add(u);                      
                    }

                }
                db.GroupModel.Add(groupmodel);
                db.SaveChanges();
                return RedirectToAction("Groups", "Home");
            }

            return View(groupmodel);
        }

        //
        // GET: /Group/Edit/5

        public ActionResult Edit(int id = 0)
        {
            GroupModel groupmodel = db.GroupModel.Find(id);
            if (groupmodel == null)
            {
                return HttpNotFound();
            }
            return View(groupmodel);
        }

        //
        // POST: /Group/Edit/5

        [HttpPost]
        public ActionResult Edit(GroupModel groupmodel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(groupmodel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(groupmodel);
        }

        //
        // POST: /Group/Join/5

   
        public ActionResult Join(int id = 0)
        { 
            GroupModel groupmodel = db.GroupModel.Find(id);
            if (groupmodel != null)
            {
                bool joined = false;
                for (int index = 0; index < groupmodel.UsersGroup.Count; index++)
                {
                    var joiners = groupmodel.UsersGroup[index];
                    if (joiners.UserName == User.Identity.Name)
                    {
                        joined = true;
                    }                    
                }
                if (joined == false)
                {
                    groupmodel.UsersGroup.Add(db.UserProfiles.FirstOrDefault(l => l.UserName == User.Identity.Name));
                }
                else
                {
                    groupmodel.UsersGroup.Remove(db.UserProfiles.FirstOrDefault(l => l.UserName == User.Identity.Name));
                }
                db.Entry(groupmodel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Groups","Home");
            }
            else
            {
                groupmodel=new GroupModel();
            }
            return View(groupmodel);
        }
        
        //
        // GET: /Group/Delete/5

        public ActionResult Delete(int id = 0)
        {
            GroupModel groupmodel = db.GroupModel.Find(id);
            if (groupmodel == null)
            {
                return HttpNotFound();
            }
            return View(groupmodel);
        }

        //
        // POST: /Group/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            GroupModel groupmodel = db.GroupModel.Find(id);
            groupmodel.UsersGroup.Remove(db.UserProfiles.FirstOrDefault(l => l.UserName == User.Identity.Name));
            db.GroupModel.Remove(groupmodel);
            db.SaveChanges();
            return RedirectToAction("Groups","Home");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}