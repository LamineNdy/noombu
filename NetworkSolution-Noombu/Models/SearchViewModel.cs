﻿using Noombu.DAL.Model;

namespace NetworkSolution_Noombu.Models
{
    public class SearchViewModel
    {
        public UserProfile UserProfile { get; set; }
        public PostModel Status { get; set; }
    }
}